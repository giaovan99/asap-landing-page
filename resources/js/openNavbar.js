document.getElementById('openMenu').style.display="flex"
document.getElementById('closeMenu').style.display="none"

$(document).ready(function(){
    $('.navbar-toggler').click(function(){
        let attr = $(this).attr('aria-expanded');
        if (attr=='true'){
            document.getElementById('openMenu').style.display="none"
            document.getElementById('closeMenu').style.display="flex"
            document.getElementById('scroll-to-top').style.visibility="hidden"
            document.getElementById('scroll-to-top').style.right="-40px"
            document.getElementById('btn-contact-mini').style.display="none"
        }else{
            document.getElementById('openMenu').style.display="flex"
            document.getElementById('closeMenu').style.display="none"
            document.getElementById('scroll-to-top').style.visibility="visible"
            document.getElementById('scroll-to-top').style.right="30px"
            document.getElementById('btn-contact-mini').style.display="block"
        }
    })
})