function backToTop (){
    window.scrollTo(0, 0);
}


var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("scroll-to-top").style.display = "none";
  } else {
    document.getElementById("scroll-to-top").style.display = "block";
  }
  prevScrollpos = currentScrollPos;
}